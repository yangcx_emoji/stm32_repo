# README #

### What is this repository for? ###

* This repository is a project collection of examples on STM32F7 serial MCU platform
* The project collection is organized according to different on chip peripherals.
* To build the project, a combination of cmake + MinGW + GNU Toolchain for ST or 
* STM32CubeIDE can be used. 
* Version 0.1.0

### How do I get set up? ###

* Clone this repo
* Install cmake and MinGW and STM32CubeIDE on windows
* Run command
1.  mkdir .\bin && mkdir .\build
2.  cd build
3.  cmake -G "MinGW Makefiles" --toolchain=..\cmake\tools\arm_stmf7.cmake ..
4.  make

* Or use the CubeIDE opening each project by double clicking the .prject file and build it seperately.
